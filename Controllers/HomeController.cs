﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SapotaCorp_AWS_Cognito.Models;

namespace SapotaCorp_AWS_Cognito.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(IFormCollection collection)
        {
            var userPoolId = "us-east-2_VIfK2NIts";
            var clientId = "306la36fqhcskj82seqfjm8pup";
            var clientSecretId = "cttssb5b7q2rn3rq9r601j32s3daefh6m11dci1g4sd7098vl9c";
            var userName = collection["username"];
            var previousPassword = collection["password"];
            var proposedPassword = collection["new-password"];

            RegionEndpoint _region = RegionEndpoint.USEast2;
            var cognito = new AmazonCognitoIdentityProviderClient(_region);
            var secretHash = CognitoHashCalculator.GetSecretHash(userName, clientId, clientSecretId);
            var request = new AdminInitiateAuthRequest
            {
                UserPoolId = userPoolId,
                ClientId = clientId,
                AuthFlow = AuthFlowType.ADMIN_NO_SRP_AUTH,
            };

            request.AuthParameters.Add("USERNAME", userName);
            request.AuthParameters.Add("PASSWORD", previousPassword);
            request.AuthParameters.Add("SECRET_HASH", secretHash);

            var response_login = await cognito.AdminInitiateAuthAsync(request);
            if(response_login.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                // error
                return View();
            }
            var request_changepass = new ChangePasswordRequest
            {
                PreviousPassword = previousPassword,
                ProposedPassword = proposedPassword,
                AccessToken = response_login.AuthenticationResult.AccessToken,
            };

            var response = await cognito.ChangePasswordAsync(request_changepass);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ValidateUser(IFormCollection collection)
        {
            var userPoolId = "us-east-2_VIfK2NIts";
            var clientId = "306la36fqhcskj82seqfjm8pup";
            var clientSecretId = "cttssb5b7q2rn3rq9r601j32s3daefh6m11dci1g4sd7098vl9c";
            var userName = collection["username"];

            RegionEndpoint _region = RegionEndpoint.USEast2;
            var cognito = new AmazonCognitoIdentityProviderClient(_region);
            var secretHash = CognitoHashCalculator.GetSecretHash(userName, clientId, clientSecretId);
            var request = new AdminConfirmSignUpRequest
            {
                Username = userName,
                UserPoolId = userPoolId,
            };
            //request.AuthParameters.Add("SECRET_HASH", secretHash);
            var response = await cognito.AdminConfirmSignUpAsync(request);
            return View();
        }

        public async Task<IActionResult> ValidateUser()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
